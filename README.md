# ewoksppf

ewoksppf provides task scheduling for cyclic [ewoks](https://ewoks.readthedocs.io/) workflows.

## Install

```bash
pip install ewoksppf[test]
```

## Test

```bash
pytest --pyargs ewoksppf.tests
```

## Documentation

https://ewoksppf.readthedocs.io/
